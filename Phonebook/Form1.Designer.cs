﻿namespace Phonebook
{
    partial class PhonebookForm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.ContactsGroupBox = new System.Windows.Forms.GroupBox();
            this.PhonebookDataGridView = new System.Windows.Forms.DataGridView();
            this.name = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.telephoneNumber = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.email = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.FilterCheckBox = new System.Windows.Forms.CheckBox();
            this.FilterTextBox = new System.Windows.Forms.TextBox();
            this.FilterLabel = new System.Windows.Forms.Label();
            this.TitleLabel = new System.Windows.Forms.Label();
            this.SelectedContactGroupBox = new System.Windows.Forms.GroupBox();
            this.UpgradePersonButton = new System.Windows.Forms.Button();
            this.RemovePerson = new System.Windows.Forms.Button();
            this.AddPersonButton = new System.Windows.Forms.Button();
            this.EmailTextBox = new System.Windows.Forms.TextBox();
            this.EmailLabel = new System.Windows.Forms.Label();
            this.TelephoneTextBox = new System.Windows.Forms.TextBox();
            this.TelephoneNumberLabel = new System.Windows.Forms.Label();
            this.NameTextBox = new System.Windows.Forms.TextBox();
            this.NameLabel = new System.Windows.Forms.Label();
            this.ContactsGroupBox.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.PhonebookDataGridView)).BeginInit();
            this.SelectedContactGroupBox.SuspendLayout();
            this.SuspendLayout();
            // 
            // ContactsGroupBox
            // 
            this.ContactsGroupBox.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.ContactsGroupBox.AutoSizeMode = System.Windows.Forms.AutoSizeMode.GrowAndShrink;
            this.ContactsGroupBox.Controls.Add(this.PhonebookDataGridView);
            this.ContactsGroupBox.Controls.Add(this.FilterCheckBox);
            this.ContactsGroupBox.Controls.Add(this.FilterTextBox);
            this.ContactsGroupBox.Controls.Add(this.FilterLabel);
            this.ContactsGroupBox.Font = new System.Drawing.Font("Times New Roman", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.ContactsGroupBox.Location = new System.Drawing.Point(26, 50);
            this.ContactsGroupBox.Name = "ContactsGroupBox";
            this.ContactsGroupBox.Size = new System.Drawing.Size(438, 290);
            this.ContactsGroupBox.TabIndex = 0;
            this.ContactsGroupBox.TabStop = false;
            this.ContactsGroupBox.Text = "Contacts ";
            // 
            // PhonebookDataGridView
            // 
            this.PhonebookDataGridView.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.PhonebookDataGridView.AutoSizeColumnsMode = System.Windows.Forms.DataGridViewAutoSizeColumnsMode.Fill;
            this.PhonebookDataGridView.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.PhonebookDataGridView.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.name,
            this.telephoneNumber,
            this.email});
            this.PhonebookDataGridView.Location = new System.Drawing.Point(6, 67);
            this.PhonebookDataGridView.Name = "PhonebookDataGridView";
            this.PhonebookDataGridView.Size = new System.Drawing.Size(426, 204);
            this.PhonebookDataGridView.TabIndex = 3;
            this.PhonebookDataGridView.RowHeaderMouseClick += new System.Windows.Forms.DataGridViewCellMouseEventHandler(this.PhonebookDataGridView_RowHeaderMouseClick);
            // 
            // name
            // 
            this.name.HeaderText = "Name";
            this.name.Name = "name";
            // 
            // telephoneNumber
            // 
            this.telephoneNumber.HeaderText = "Telephone number";
            this.telephoneNumber.Name = "telephoneNumber";
            // 
            // email
            // 
            this.email.HeaderText = "e-mail";
            this.email.Name = "email";
            // 
            // FilterCheckBox
            // 
            this.FilterCheckBox.AutoSize = true;
            this.FilterCheckBox.Location = new System.Drawing.Point(324, 25);
            this.FilterCheckBox.Name = "FilterCheckBox";
            this.FilterCheckBox.Size = new System.Drawing.Size(15, 14);
            this.FilterCheckBox.TabIndex = 2;
            this.FilterCheckBox.UseVisualStyleBackColor = true;
            // 
            // FilterTextBox
            // 
            this.FilterTextBox.Font = new System.Drawing.Font("Times New Roman", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.FilterTextBox.Location = new System.Drawing.Point(81, 19);
            this.FilterTextBox.Multiline = true;
            this.FilterTextBox.Name = "FilterTextBox";
            this.FilterTextBox.Size = new System.Drawing.Size(234, 24);
            this.FilterTextBox.TabIndex = 1;
            this.FilterTextBox.TextChanged += new System.EventHandler(this.FilterTextBox_TextChanged);
            // 
            // FilterLabel
            // 
            this.FilterLabel.AutoSize = true;
            this.FilterLabel.BackColor = System.Drawing.Color.Aqua;
            this.FilterLabel.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.FilterLabel.Font = new System.Drawing.Font("Times New Roman", 14.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.FilterLabel.Location = new System.Drawing.Point(6, 19);
            this.FilterLabel.Name = "FilterLabel";
            this.FilterLabel.Size = new System.Drawing.Size(72, 24);
            this.FilterLabel.TabIndex = 0;
            this.FilterLabel.Text = "Filter : ";
            // 
            // TitleLabel
            // 
            this.TitleLabel.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.TitleLabel.AutoSize = true;
            this.TitleLabel.BackColor = System.Drawing.Color.DeepSkyBlue;
            this.TitleLabel.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.TitleLabel.Font = new System.Drawing.Font("Times New Roman", 15.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.TitleLabel.Location = new System.Drawing.Point(384, 9);
            this.TitleLabel.Name = "TitleLabel";
            this.TitleLabel.Size = new System.Drawing.Size(113, 26);
            this.TitleLabel.TabIndex = 1;
            this.TitleLabel.Text = "Phonebook";
            // 
            // SelectedContactGroupBox
            // 
            this.SelectedContactGroupBox.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.SelectedContactGroupBox.AutoSizeMode = System.Windows.Forms.AutoSizeMode.GrowAndShrink;
            this.SelectedContactGroupBox.Controls.Add(this.UpgradePersonButton);
            this.SelectedContactGroupBox.Controls.Add(this.RemovePerson);
            this.SelectedContactGroupBox.Controls.Add(this.AddPersonButton);
            this.SelectedContactGroupBox.Controls.Add(this.EmailTextBox);
            this.SelectedContactGroupBox.Controls.Add(this.EmailLabel);
            this.SelectedContactGroupBox.Controls.Add(this.TelephoneTextBox);
            this.SelectedContactGroupBox.Controls.Add(this.TelephoneNumberLabel);
            this.SelectedContactGroupBox.Controls.Add(this.NameTextBox);
            this.SelectedContactGroupBox.Controls.Add(this.NameLabel);
            this.SelectedContactGroupBox.Font = new System.Drawing.Font("Times New Roman", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.SelectedContactGroupBox.Location = new System.Drawing.Point(518, 50);
            this.SelectedContactGroupBox.Name = "SelectedContactGroupBox";
            this.SelectedContactGroupBox.Size = new System.Drawing.Size(279, 287);
            this.SelectedContactGroupBox.TabIndex = 2;
            this.SelectedContactGroupBox.TabStop = false;
            this.SelectedContactGroupBox.Text = "Selected contact data";
            // 
            // UpgradePersonButton
            // 
            this.UpgradePersonButton.Anchor = System.Windows.Forms.AnchorStyles.None;
            this.UpgradePersonButton.AutoSizeMode = System.Windows.Forms.AutoSizeMode.GrowAndShrink;
            this.UpgradePersonButton.Location = new System.Drawing.Point(189, 241);
            this.UpgradePersonButton.Name = "UpgradePersonButton";
            this.UpgradePersonButton.Size = new System.Drawing.Size(37, 25);
            this.UpgradePersonButton.TabIndex = 8;
            this.UpgradePersonButton.UseVisualStyleBackColor = true;
            this.UpgradePersonButton.Click += new System.EventHandler(this.UpgradePersonButton_Click);
            // 
            // RemovePerson
            // 
            this.RemovePerson.Anchor = System.Windows.Forms.AnchorStyles.None;
            this.RemovePerson.AutoSizeMode = System.Windows.Forms.AutoSizeMode.GrowAndShrink;
            this.RemovePerson.Font = new System.Drawing.Font("Times New Roman", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.RemovePerson.Location = new System.Drawing.Point(123, 241);
            this.RemovePerson.Name = "RemovePerson";
            this.RemovePerson.Size = new System.Drawing.Size(40, 25);
            this.RemovePerson.TabIndex = 7;
            this.RemovePerson.Text = "X";
            this.RemovePerson.UseVisualStyleBackColor = true;
            this.RemovePerson.Click += new System.EventHandler(this.RemovePerson_Click);
            // 
            // AddPersonButton
            // 
            this.AddPersonButton.Anchor = System.Windows.Forms.AnchorStyles.None;
            this.AddPersonButton.AutoSizeMode = System.Windows.Forms.AutoSizeMode.GrowAndShrink;
            this.AddPersonButton.Font = new System.Drawing.Font("Times New Roman", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.AddPersonButton.Location = new System.Drawing.Point(58, 241);
            this.AddPersonButton.Name = "AddPersonButton";
            this.AddPersonButton.Size = new System.Drawing.Size(38, 26);
            this.AddPersonButton.TabIndex = 6;
            this.AddPersonButton.Text = "+";
            this.AddPersonButton.UseVisualStyleBackColor = true;
            this.AddPersonButton.Click += new System.EventHandler(this.AddPersonButton_Click);
            // 
            // EmailTextBox
            // 
            this.EmailTextBox.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Left | System.Windows.Forms.AnchorStyles.Right)));
            this.EmailTextBox.Font = new System.Drawing.Font("Times New Roman", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.EmailTextBox.Location = new System.Drawing.Point(25, 202);
            this.EmailTextBox.Multiline = true;
            this.EmailTextBox.Name = "EmailTextBox";
            this.EmailTextBox.Size = new System.Drawing.Size(248, 23);
            this.EmailTextBox.TabIndex = 5;
            // 
            // EmailLabel
            // 
            this.EmailLabel.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Left | System.Windows.Forms.AnchorStyles.Right)));
            this.EmailLabel.AutoSize = true;
            this.EmailLabel.Font = new System.Drawing.Font("Times New Roman", 14.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.EmailLabel.Location = new System.Drawing.Point(21, 177);
            this.EmailLabel.Name = "EmailLabel";
            this.EmailLabel.Size = new System.Drawing.Size(75, 22);
            this.EmailLabel.TabIndex = 4;
            this.EmailLabel.Text = "e-mail : ";
            // 
            // TelephoneTextBox
            // 
            this.TelephoneTextBox.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Left | System.Windows.Forms.AnchorStyles.Right)));
            this.TelephoneTextBox.Font = new System.Drawing.Font("Times New Roman", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.TelephoneTextBox.Location = new System.Drawing.Point(25, 135);
            this.TelephoneTextBox.Multiline = true;
            this.TelephoneTextBox.Name = "TelephoneTextBox";
            this.TelephoneTextBox.Size = new System.Drawing.Size(248, 22);
            this.TelephoneTextBox.TabIndex = 3;
            // 
            // TelephoneNumberLabel
            // 
            this.TelephoneNumberLabel.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Left | System.Windows.Forms.AnchorStyles.Right)));
            this.TelephoneNumberLabel.AutoSize = true;
            this.TelephoneNumberLabel.Font = new System.Drawing.Font("Times New Roman", 14.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.TelephoneNumberLabel.Location = new System.Drawing.Point(21, 110);
            this.TelephoneNumberLabel.Name = "TelephoneNumberLabel";
            this.TelephoneNumberLabel.Size = new System.Drawing.Size(175, 22);
            this.TelephoneNumberLabel.TabIndex = 2;
            this.TelephoneNumberLabel.Text = "Telephone number : ";
            // 
            // NameTextBox
            // 
            this.NameTextBox.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Left | System.Windows.Forms.AnchorStyles.Right)));
            this.NameTextBox.Font = new System.Drawing.Font("Times New Roman", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.NameTextBox.Location = new System.Drawing.Point(25, 67);
            this.NameTextBox.Multiline = true;
            this.NameTextBox.Name = "NameTextBox";
            this.NameTextBox.Size = new System.Drawing.Size(248, 19);
            this.NameTextBox.TabIndex = 1;
            // 
            // NameLabel
            // 
            this.NameLabel.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Left | System.Windows.Forms.AnchorStyles.Right)));
            this.NameLabel.AutoSize = true;
            this.NameLabel.Font = new System.Drawing.Font("Times New Roman", 14.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.NameLabel.Location = new System.Drawing.Point(21, 42);
            this.NameLabel.Name = "NameLabel";
            this.NameLabel.Size = new System.Drawing.Size(106, 22);
            this.NameLabel.TabIndex = 0;
            this.NameLabel.Text = "Full name : ";
            // 
            // PhonebookForm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.Color.LightBlue;
            this.ClientSize = new System.Drawing.Size(882, 365);
            this.Controls.Add(this.SelectedContactGroupBox);
            this.Controls.Add(this.TitleLabel);
            this.Controls.Add(this.ContactsGroupBox);
            this.Name = "PhonebookForm";
            this.Text = "Phonebook";
            this.Load += new System.EventHandler(this.PhonebookForm_Load);
            this.ContactsGroupBox.ResumeLayout(false);
            this.ContactsGroupBox.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.PhonebookDataGridView)).EndInit();
            this.SelectedContactGroupBox.ResumeLayout(false);
            this.SelectedContactGroupBox.PerformLayout();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.GroupBox ContactsGroupBox;
        private System.Windows.Forms.DataGridView PhonebookDataGridView;
        private System.Windows.Forms.CheckBox FilterCheckBox;
        private System.Windows.Forms.TextBox FilterTextBox;
        private System.Windows.Forms.Label FilterLabel;
        private System.Windows.Forms.Label TitleLabel;
        private System.Windows.Forms.DataGridViewTextBoxColumn name;
        private System.Windows.Forms.DataGridViewTextBoxColumn telephoneNumber;
        private System.Windows.Forms.DataGridViewTextBoxColumn email;
        private System.Windows.Forms.GroupBox SelectedContactGroupBox;
        private System.Windows.Forms.TextBox EmailTextBox;
        private System.Windows.Forms.Label EmailLabel;
        private System.Windows.Forms.TextBox TelephoneTextBox;
        private System.Windows.Forms.Label TelephoneNumberLabel;
        private System.Windows.Forms.TextBox NameTextBox;
        private System.Windows.Forms.Label NameLabel;
        private System.Windows.Forms.Button UpgradePersonButton;
        private System.Windows.Forms.Button RemovePerson;
        private System.Windows.Forms.Button AddPersonButton;
    }
}

