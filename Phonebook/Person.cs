﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Phonebook
{
    class Person
    {
        private string name;
        private string telNumber;
        private string email;
        public string getName()
        {
            return this.name;
        }

        public void setName(string nameValue)
        {
            this.name = nameValue;
        }

        public string getTelephoneNumber()
        {
            return this.telNumber;
        }

        public void setTelephoneNumber(string telNumberValue)
        {
            this.telNumber = telNumberValue;
        }

        public string getEmail()
        {
            return this.email;
        }

        public void setEmail(string emailValue)
        {
            this.email = emailValue;
        }
    }
}
