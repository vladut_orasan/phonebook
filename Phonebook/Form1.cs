﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.IO;
using System.Text.RegularExpressions;

namespace Phonebook
{
    public partial class PhonebookForm : Form
    {
        List<Person> persons = new List<Person>();
        string filePath = "Phonebook.txt";
        public PhonebookForm()
        {
            InitializeComponent();
            UpgradePersonButton.Text = "\u221A";
        }

        private void ImportData()
        {
            string[] temporary = new string[3];
            try
            {
                using (StreamReader streamReader = new StreamReader(filePath))
                {
                    string line;
                    while (!(line = streamReader.ReadLine()).Equals(""))
                    {
                        Person newPerson = new Person();
                        temporary = line.Split(',');
                        newPerson.setName(temporary[0]);
                        newPerson.setTelephoneNumber(temporary[1]);
                        newPerson.setEmail(temporary[2]);
                        persons.Add(newPerson);
                    }
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
            }
            foreach (Person person in persons)
            {
                PhonebookDataGridView.Rows.Add(person.getName(), person.getTelephoneNumber(), person.getEmail());
            }
        }

        private void PhonebookForm_Load(object sender, EventArgs e)
        {
            ImportData();
        }

        private void ClearData()
        {
            NameTextBox.Text = "";
            TelephoneTextBox.Text = "";
            EmailTextBox.Text = "";
        }
        /*
        private void PhonebookDataGridView_SelectionChanged(object sender, EventArgs e)
        {
            foreach (DataGridViewRow row in PhonebookDataGridView.SelectedRows)
            {
                string name = row.Cells[0].Value.ToString();
                NameTextBox.Text = name;
                string telephoneNumber = row.Cells[1].Value.ToString();
                TelephoneTextBox.Text = telephoneNumber;
                string email = row.Cells[2].Value.ToString();
                EmailTextBox.Text = email;
            }
        }
        */
        
        private void PhonebookDataGridView_RowHeaderMouseClick(object sender, DataGridViewCellMouseEventArgs e)
        {
            NameTextBox.Text = PhonebookDataGridView.Rows[e.RowIndex].Cells[0].Value.ToString();
            TelephoneTextBox.Text = PhonebookDataGridView.Rows[e.RowIndex].Cells[1].Value.ToString();
            EmailTextBox.Text = PhonebookDataGridView.Rows[e.RowIndex].Cells[2].Value.ToString();
        }

        private bool isValidName(string name)
        {
            Regex regexExpression = new Regex(@"[\p{L}\p{N}_]");
            return regexExpression.IsMatch(name);
        }

        private bool isValidPhoneNumber(string phoneNumber)
        {
            Regex regexExpression = new Regex(@"^(?<paren>\()?0(?:(?:72|74|75|76|77|78)(?(paren)\))(?<first>\d)(?!\k<first>{6})\d{6}|(?:251|351)(?(paren)\))(?<first>\d)(?!\k<first>{5})\d{5})$");
            return regexExpression.IsMatch(phoneNumber);
        }

        private bool isValidEmail(string email)
        {
            Regex regexExpression = new Regex(@"^(?("")("".+?(?<!\\)""@)|(([0-9a-z]((\.(?!\.))|[-!#\$%&'\*\+/=\?\^`\{\}\|~\w])*)(?<=[0-9a-z])@))");
            return regexExpression.IsMatch(email);
        }

        private void InsertContact()
        {
            if (NameTextBox.Text != "" && TelephoneTextBox.Text != "" && EmailTextBox.Text != "")
            {
                if (isValidName(NameTextBox.Text))
                {
                    if (isValidPhoneNumber(TelephoneTextBox.Text))
                    {
                        if (isValidEmail(EmailTextBox.Text))
                        {
                            int id = 0;
                            int dim = PhonebookDataGridView.Rows.Count;
                            for (int i = 0; i < dim; i++)
                            {
                                string test = Convert.ToString(PhonebookDataGridView.Rows[i].Cells[0].Value);
                                if (test != "")
                                {
                                    id++;
                                }
                            }
                            PhonebookDataGridView.Rows.Add(1);
                            PhonebookDataGridView.Rows[id].Cells[0].Value = NameTextBox.Text;
                            PhonebookDataGridView.Rows[id].Cells[1].Value = TelephoneTextBox.Text;
                            PhonebookDataGridView.Rows[id].Cells[2].Value = EmailTextBox.Text;
                            Person newPerson = new Person();
                            newPerson.setName(NameTextBox.Text);
                            newPerson.setTelephoneNumber(TelephoneTextBox.Text);
                            newPerson.setEmail(EmailTextBox.Text);
                            persons.Add(newPerson);
                            MessageBox.Show("Record inserted successfully ! ");
                            ClearData();
                            PhonebookDataGridView.Refresh();
                            //Write last line in text file
                            using (StreamWriter sw = new StreamWriter(filePath, true))
                            {
                                sw.WriteLine(persons[persons.Count - 1].getName() + ',' + persons[persons.Count - 1].getTelephoneNumber() + ',' + persons[persons.Count - 1].getEmail());
                            }
                        }
                        else
                        {
                            MessageBox.Show("Invalid e-mail address !");
                        }
                    }
                    else
                    {
                        MessageBox.Show("Invalid phone number ! ");
                    }
                }
                else
                {
                    MessageBox.Show("Invalid name entered ! ");
                }
            }
            else
            {
                MessageBox.Show("Please provide details ! ");
            }
        }

        private void AddPersonButton_Click(object sender, EventArgs e)
        {
            InsertContact();
        }

        private void RemoveContact()
        {
            if (NameTextBox.Text != "" && TelephoneTextBox.Text != "" && EmailTextBox.Text != "")
            {
                int selectedRow;
                selectedRow = PhonebookDataGridView.SelectedRows[0].Index;
                if (PhonebookDataGridView.SelectedRows.Count > 0)
                {
                    PhonebookDataGridView.Rows.RemoveAt(selectedRow);
                }
                MessageBox.Show("Record deleted successfully ! ");
                ClearData();
                PhonebookDataGridView.Refresh();
                persons.RemoveAt(selectedRow);

                //Removing user from the "database"
                using (StreamWriter sw = new StreamWriter(filePath))
                {
                    for (int i = persons.Count; i > 0; i--)
                    {
                        sw.Write(persons[persons.Count - i].getName());
                        sw.Write(',');
                        sw.Write(persons[persons.Count - i].getTelephoneNumber());
                        sw.Write(',');
                        sw.Write(persons[persons.Count - i].getEmail());
                        sw.Write("\n");
                    }
                }
            }
            else
            {
                MessageBox.Show("Please provide details ! ");
            }
        }

        private void RemovePerson_Click(object sender, EventArgs e)
        {
            RemoveContact();
        }

        private void UpgradeContact()
        {
            if (NameTextBox.Text != "" && TelephoneTextBox.Text != "" && EmailTextBox.Text != "")
            {
                if (isValidName(NameTextBox.Text))
                {
                    if (isValidPhoneNumber(TelephoneTextBox.Text))
                    {
                        if (isValidEmail(EmailTextBox.Text))
                        {
                            try
                            {
                                int selectedRow;
                                selectedRow = PhonebookDataGridView.SelectedRows[0].Index;
                                PhonebookDataGridView.Rows[selectedRow].SetValues(NameTextBox.Text, TelephoneTextBox.Text, EmailTextBox.Text);
                                persons[selectedRow].setName(NameTextBox.Text);
                                persons[selectedRow].setTelephoneNumber(TelephoneTextBox.Text);
                                persons[selectedRow].setEmail(EmailTextBox.Text);
                                PhonebookDataGridView.Refresh();
                                using (StreamWriter sw = new StreamWriter(filePath))
                                {
                                    for (int i = persons.Count; i > 0; i--)
                                    {
                                        sw.Write(persons[persons.Count - i].getName());
                                        sw.Write(',');
                                        sw.Write(persons[persons.Count - i].getTelephoneNumber());
                                        sw.Write(',');
                                        sw.Write(persons[persons.Count - i].getEmail());
                                        sw.Write("\n");
                                    }
                                }
                                ClearData();
                                MessageBox.Show("Contact updated ! ");
                            }
                            catch (Exception ex)
                            {
                                MessageBox.Show(ex.Message);
                            }
                        }
                        else
                        {
                            MessageBox.Show("Invalid e-mail address !");
                        }
                    }
                    else
                    {
                        MessageBox.Show("Invalid phone number ! ");
                    }
                }
                else
                {
                    MessageBox.Show("Invalid name entered ! ");
                }
            }
            else
            {
                MessageBox.Show("Please provide details ! ");
            }
        }
        
        private void UpgradePersonButton_Click(object sender, EventArgs e)
        {
            UpgradeContact();
        }

        private void FilterTextBox_TextChanged(object sender, EventArgs e)
        {
            PhonebookDataGridView.DataSource = null;
            PhonebookDataGridView.Rows.Clear();
            PhonebookDataGridView.Refresh();
            foreach (Person person in persons)
            {
                if (person.getName().Contains(FilterTextBox.Text) || person.getTelephoneNumber().Contains(FilterTextBox.Text) || person.getEmail().Contains(FilterTextBox.Text))
                {
                    PhonebookDataGridView.Rows.Add(person.getName(), person.getTelephoneNumber(), person.getEmail());
                }
            }
            /*
            BindingSource FilterBindingSource = new BindingSource();
            FilterBindingSource.DataSource = PhonebookDataGridView.DataSource;
            FilterBindingSource.Filter = string.Format("CONVERT(" + PhonebookDataGridView.Columns[0].DataPropertyName + ", System.String) like '%" + FilterTextBox.Text.Replace("'", "''") + "%'");
            PhonebookDataGridView.DataSource = FilterBindingSource;
            */
            /*
            if (FilterCheckBox.Checked == true)
            {
                try
                {
                    ((DataTable)PhonebookDataGridView.DataSource).DefaultView.RowFilter = "Name like'" + FilterTextBox.Text.Trim().Replace("'", "''") + "%'";
                }
                catch (Exception ex)
                {
                    MessageBox.Show(ex.Message);
                }
            }
            */
        }
    }
}
